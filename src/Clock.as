package
{
	import flash.display.Sprite;
	
	public class Clock extends Sprite
	{
		private var _hour:uint;
		private var _min:uint;
		
		public function get hour():uint { return _hour; }
		public function get min():uint { return _min; }
		
		public function Clock()
		{
			
		}
		
		public function at(hours:uint, mins:uint=0):String
		{
			_hour = hours;
			_min = mins;
			
			return formattedString();
		}
		
		private function formattedString():String
		{
			var hourStr:String = _hour.toString();
			hourStr = (hourStr.length==1)?'0'+hourStr : hourStr;
			var minStr:String = _min.toString();
			minStr = (minStr.length==1)?'0'+minStr : minStr;
			
			return hourStr + ':' + minStr;
		}
		
		public function plus(mins:uint):String
		{
			
			// if less than 60, add straight away
			if(mins<60)
			{
				_min += mins;
			}
			else
			{
				_hour += mins/60; // add the hours
				_min = mins ^ 60; // mins will be equal to the remainder
			}
			
			// if min is over 60, inc. hrs
			if(min>60)
			{
				_min -= 60;
				_hour ++;
			}
			// if hr is over midnight reset it to 0
			if(_hour>23)
				_hour = 0;
			
			//trace(formattedString());
			return formattedString();
		}
		
		public function equals(anotherClock:Clock):Boolean
		{
			if(_hour == anotherClock.hour && _min == anotherClock.min)
				return true;
			
			return false;
		}
	}
}